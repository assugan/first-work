terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "3.5.0"
    }
  }
   backend "http" {
  }
}

provider "google" {

  project = "optimal-iris-342021"
  region  = "europe-west3"
  #zone    = "europe-west3-a"
}


resource "google_compute_network" "network" {
  name = "primary"
}


resource "google_container_cluster" "primary" {
  name                     = var.cluster_name
  location                 = var.gcp_region
  node_locations = [
    "europe-west3-a"
  ]
  remove_default_node_pool = true
  initial_node_count       = 1
  min_master_version       = "1.20"
  description              = var.cluster_description
  network                  = google_compute_network.network.name
}

resource "google_container_node_pool" "primary_nodes" {
  name       = "${var.cluster_name}-node-pool"
  cluster    = google_container_cluster.primary.name
  location   = var.gcp_region
  node_count = var.node_count

  node_config {
    preemptible  = false
    machine_type = var.machine_type

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}
